using System;

// Pour gérer l'état du jeu
public class PlayerEventArgs : EventArgs
{
    public PlayerModel Model { get; set; }

    public PlayerEventArgs(PlayerModel model)
    {
        Model = model;
    }
}