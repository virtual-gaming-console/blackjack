/*
 * Permet de mettre en place la gestion des évènements liés aux cartes
*/

public delegate void CardEventHandler(object sender, CardEventArgs e);