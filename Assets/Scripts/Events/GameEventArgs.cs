using System;


// Pour gérer l'état du jeu
public class GameEventArgs : EventArgs
{
    public bool GameIsOver { get; set; }

    public GameEventArgs(bool gameIsOver)
    {
        GameIsOver = gameIsOver;
    }
}