using System;

/*
 * S'occupe de garder en mémoire quel est l'index à traiter pour les events liés aux cartes
 *
*/
public class CardEventArgs : EventArgs
{
    public int CardIndex { get; set; }

    public CardEventArgs(int cardIndex)
    {
        CardIndex = cardIndex;
    }
}