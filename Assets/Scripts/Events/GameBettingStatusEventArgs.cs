using System;


// Pour gérer l'état du jeu
public class GameBettingStatusEventArgs : EventArgs
{
    public bool Betting { get; set; }

    public GameBettingStatusEventArgs(bool betting)
    {
        Betting = betting;
    }
}