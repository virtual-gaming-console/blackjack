// Gérer les évènements liés à l'état actuel du jeu
public delegate void GameActionHandler(GameActionArgs e);