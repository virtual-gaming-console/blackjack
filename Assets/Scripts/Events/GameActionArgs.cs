using System;

// Pour gérer l'état du jeu
public class GameActionArgs : EventArgs
{
    public WSMessage WsMessage { get; set; }

    public GameActionArgs(WSMessage wsMessage)
    {
        WsMessage = wsMessage;
    }
}