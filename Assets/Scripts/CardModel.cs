﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Model pour les cartes
*/
public class CardModel : MonoBehaviour
{
    SpriteRenderer spriteRenderer;

    public Sprite[] faces;
    public Sprite cardBack;

    public int CardIndex;

    // Affiche la face visbile de la carte avec true ou bien le dos avec false
    public void ToggleFace(bool showFace)
    {
        if (showFace)
        {
            spriteRenderer.sprite = faces[CardIndex];
        }
        else
        {
            spriteRenderer.sprite = cardBack;
        }
    }

    // Executé avant la première frame
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

}