﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * S'occupe de gérer le flip des cartes
 * Ce composant est intégré dans l'objet Card
*/

public class CardFlipper : MonoBehaviour
{
    SpriteRenderer spriteRenderer;
    CardModel model;

    public AnimationCurve scaleCurve;
    public float duration = 0.5f;

    // Exécuté avant la première frame
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        model = GetComponent<CardModel>();
    }

    // Démarre l'action de retourner une carte
    public void FlipCard(Sprite startImage, Sprite endImage, int cardIndex)
    {
        StopCoroutine(Flip(startImage, endImage, cardIndex));
        StartCoroutine(Flip(startImage, endImage, cardIndex));
    }

    // Donne l'impression de retourner la carte en utilisant un vecteur 
    // qui écrase la carte puis la réagrandit avec l'image correspondante
    IEnumerator Flip(Sprite startImage, Sprite endImage, int cardIndex)
    {
        spriteRenderer.sprite = startImage;
        float time = 0f;
        while (time <= 1f)
        {
            float scale = scaleCurve.Evaluate(time);
            time = time + Time.deltaTime / duration;

            Vector3 localScale = transform.localScale;
            localScale.x = scale;
            transform.localScale = localScale;

            if (time >= 0.5f)
            {
                spriteRenderer.sprite = endImage;
            }

            yield return new WaitForFixedUpdate();
        }

        if (cardIndex == -1)
        {
            model.ToggleFace(false);
        }
        else
        {
            model.CardIndex = cardIndex;
            model.ToggleFace(true);
        }
    }
}
