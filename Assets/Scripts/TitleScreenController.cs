﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using NativeWebSocket;
using System.Net.Sockets;
using UnityEngine.Networking;

public class TitleScreenController : MonoBehaviour
{
    // public string ip = "192.168.0.16:1337";
    public string ip = "api.playdeviant.com";
    public string lobbyCode;
    List<PlayerModel> listOfPlayerModel;
    public GameObject PlayerLoaderPrefab;
    List<GameObject> InstantiatedPrefabs = new List<GameObject>();
    Dictionary<string, GameObject> DictionnaryIdPrefab = new Dictionary<string, GameObject>();

    public void LaunchGame(bool asController)
    {
        RegisterPlayers();
        if (!asController)
        {
            SceneManager.LoadScene("BlackJackGame");
        }
        else
        {
            SceneManager.LoadScene("Player Scene");
        }
    }

    public void LaunchGameAsScreen()
    {
        RegisterPlayers();
        SceneManager.LoadScene("BlackJackGame");
    }

    public void AddPlayerLoader(string memberId, float x, float y)
    {
        DictionnaryIdPrefab.Add(memberId, (GameObject)Instantiate(PlayerLoaderPrefab));
        PlayerLoaderController loaderController = DictionnaryIdPrefab[memberId].GetComponent<PlayerLoaderController>();
        loaderController.Panel.transform.position = new Vector3(x, y, 0);

        var playerModel = listOfPlayerModel.Find(player => player.LobbyMemberID == memberId);
        loaderController.TextPseudo.text = playerModel.Pseudo;
        StartCoroutine(GameProperties.SetAvatarFromUrl(loaderController.Avatar, playerModel));
    }

    void RegisterPlayers()
    {
        GameProperties.InitiateListOfPlayers(this.listOfPlayerModel);
    }

    void Awake()
    {

    }

    void Start()
    {
#if UNITY_EDITOR
        ConnectToLobby(lobbyCode);
#endif
    }

    public void ConnectToLobby(string lobbyCode)
    {
        GameProperties.isController = false;
        InitiateWebSocketConnection(lobbyCode, false, null);
    }

    public void OpenPlayerScene(string lobbyCodeAndPlayerId)
    {
        GameProperties.isController = true;


        string[] splitedString = lobbyCodeAndPlayerId.Split('-');
        string lobbyCode = splitedString[0];
        string playerId = splitedString[1];

        GameProperties.ActualPlayerId = playerId;
        InitiateWebSocketConnection(lobbyCode, true, playerId);
    }

    public async void InitiateWebSocketConnection(string lobbyCode, bool asController, string memberId)
    {
        // Create WebSocket instance
        GameProperties.ws = new WebSocket($"wss://{ip}");

        // Add OnOpen event listener
        GameProperties.ws.OnOpen += () =>
        {
            if (memberId != null)
            {
                GameProperties.SendMessage<JoinRoomMessage>(new JoinRoomMessage(lobbyCode, asController, memberId));
            }
            else
            {
                GameProperties.SendMessage<JoinRoomMessage>(new JoinRoomMessage(lobbyCode, asController));
            }

        };

        // Add OnMessage event listener
        GameProperties.ws.OnMessage += (byte[] msg) =>
        {
            WSMessage mess = JsonUtility.FromJson<WSMessage>(Encoding.UTF8.GetString(msg, 0, msg.Length));
            Debug.Log(mess);
            if (mess.title == "join-room-success")
            {
                StartCoroutine(RetreiveLobbyInfos2(lobbyCode, asController));
            }
            else if (mess.title == "controller-action" && mess.data.action == "controller-initialized")
            {
                InitializeController(mess);
            }
            else
            {
                GameProperties.Action(mess);
            }
        };

        // Add OnError event listener
        // GameProperties.ws.OnError += (e) =>
        // {
        //     Debug.Log("WS error: " + e);
        // };

        // Add OnClose event listener
        GameProperties.ws.OnClose += (e) =>
        {
            Debug.Log("WS closed");
        };

        // Connect to the server
        await GameProperties.ws.Connect();
    }

    public IEnumerator RetreiveLobbyInfos2(string lobbyCode, bool asController)
    {
        Lobby lobby;
        string Url = $"https://{ip}/lobby/{lobbyCode}";

        using (UnityWebRequest webRequest = UnityWebRequest.Get(Url))
        {
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                Debug.Log("Error: " + webRequest.error);
            }
            else
            {
                lobby = JsonUtility.FromJson<Lobby>(webRequest.downloadHandler.text);
                if (lobby != null)
                {
                    listOfPlayerModel = new List<PlayerModel>();
                    Debug.Log(lobby);
                    for (int i = 0; i < lobby.members.Count; i++)
                    {
                        PlayerModel newPlayer = new PlayerModel(i + 1, lobby.members[i].nickname);
                        newPlayer.LobbyMemberID = lobby.members[i].id;
                        newPlayer.AvatarUrl = lobby.members[i].picture;
                        listOfPlayerModel.Add(newPlayer);
                        if (!asController)
                        {
                            ScreenActionMessage mess = new ScreenActionMessage("open-controller", newPlayer.LobbyMemberID, "launchwithparams://ontestdesbails");
                            GameProperties.SendMessage<ScreenActionMessage>(mess);
                        }
                    }
                    // Initialiser les loaders
                    if (asController)
                    {
                        LaunchGame(asController);
                    }
                    else
                    {
                        InitializeLoaders();
                    }
                }
            }
        }
    }

    // Place les loaders de chaque joueur présent dans la room
    public void InitializeLoaders()
    {
        float midY = Screen.height / 2;

        for (int i = 0; i < listOfPlayerModel.Count; i++)
        {
            AddPlayerLoader(listOfPlayerModel[i].LobbyMemberID, (Screen.width / (listOfPlayerModel.Count + 1)) * (i + 1), midY);
        }
    }

    void InitializeController(WSMessage mess)
    {
        var player = listOfPlayerModel.Find(x => x.LobbyMemberID == mess.data.member.id);
        player.IsConnected = true;
        DictionnaryIdPrefab[mess.data.member.id].GetComponent<PlayerLoaderController>().ControllerConnected();
        StartCoroutine(CheckControllerInit());
    }

    bool AreAllControllerInit()
    {
        for (int i = 0; i < listOfPlayerModel.Count; i++)
        {
            var model = listOfPlayerModel[i];
            if (!model.IsConnected)
            {
                return false;
            }
        }
        return true;
    }

    IEnumerator CheckControllerInit()
    {
        Debug.Log("CheckControllerInit");
        if (AreAllControllerInit())
        {
            Debug.Log("AreAllControllerInit");
            yield return new WaitForSeconds(1);
            LaunchGame(false);
        }
    }
}
