﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DefeatOverlayController : MonoBehaviour
{
    public Text LoserPseudo;
    public RawImage LoserAvatar;
    public GameObject LoseMessagePrefab;
    public GameObject InstantiatedPrefab;

    public Button restartBtn;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Canvas>().worldCamera = Camera.main;
        GetComponent<Canvas>().sortingLayerName = "FrontLayer";

        if (GameProperties.ActualPlayerId != null)
        {
            PlayerModel player = GameProperties.GetPlayerModelWithMemberID(GameProperties.ActualPlayerId);
            LoserPseudo.text = player.Pseudo;
            StartCoroutine(GameProperties.SetAvatarFromUrl(LoserAvatar, player));
        }

        InstantiatedPrefab = Instantiate(LoseMessagePrefab);
        InstantiatedPrefab.GetComponent<MeshRenderer>().sortingLayerName = "FrontLayer";
        restartBtn.onClick.AddListener(delegate { RestartGame(); });
    }

    public void RestartGame()
    {
        if (GameProperties.isController)
        {
            GameProperties.SendMessage<ControllerActionMessage>(new ControllerActionMessage("restart-game", GameProperties.ActualPlayerId, null));
        }
    }

    void OnDestroy()
    {
        Destroy(this.InstantiatedPrefab);
    }
}
