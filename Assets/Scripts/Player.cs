
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Modèle pour les joueurss
public class Player : MonoBehaviour
{
    public CardStack Hand;
    PlayerModel _model;
    public PlayerModel Model
    {
        get => _model;
        set
        {
            if (value != null)
            {

                _model = value;
                _model.PlayerUpdate += Player_Update;
                UpdatePlayerData();
            }
            else
            {
                _model.PlayerUpdate -= Player_Update;
                _model = value;
            }
        }
    }
    public Text textScore;
    public Text textPseudo;
    public GameObject MoneyComponent;
    public GameObject PlayerArea;
    public float xAdjust = .8f;
    public bool isController = false;
    public RawImage Avatar;

    public override string ToString()
    {
        return $"{Model.Pseudo}: {Model.Money}: {Model.PlayerID}";
    }

    void Start()
    {
        Hand = GetComponent<CardStack>();
        if (!isController)
        {
            GetComponent<CardStackView>().start = new Vector3(this.transform.position.x - .5f, this.transform.position.y, 0);
        }
    }

    // Attention model doit exister pour faire ça
    void Awake()
    {
        Model = new PlayerModel();

        // Link l'event depuis PlayerModel
        Model.PlayerUpdate += Player_Update;
    }

    // Ajoute une carte à la main et update le score
    public void Push(int card)
    {
        Hand.Push(card);
        Model.Score = Hand.HandValue();
    }

    // Le joueur à dépassé 21 : il est éliminé pour cette manche
    public void Burst()
    {
        ResetBet();
        GameObject prefabBurst = Hand.textBustedPrefab;
        float ajustement = 0.8f;
        Model.isBurst = true;
        Hand.InstantiatedPrefab = (GameObject)Instantiate(prefabBurst, new Vector3(transform.position.x, transform.position.y, ajustement), prefabBurst.transform.rotation);
        Hand.InstantiatedPrefab.GetComponent<MeshRenderer>().sortingOrder = 9;
        Hand.InstantiatedPrefab.GetComponent<MeshRenderer>().sortingLayerName = "MiddleFront";
        if (isController)
        {
            Hand.InstantiatedPrefab.GetComponent<TextMesh>().characterSize = 0.1f;
        }
        StartCoroutine(BurstSound());
    }

    // Joue le son correspondant
    IEnumerator BurstSound()
    {
        yield return new WaitForSeconds(0.33f);
        SoundManager.PlaySound("busted");
    }

    void ResetBet()
    {
        Model.ResetBet();

        // Si le joueur n'a plus de money il est tagué "out"
        if (Model.Money == 0)
        {
            Model.Out = true;
        }
    }

    void Pay(int amount)
    {
        Model.Money += amount;
    }

    // Quand le joueur mise, la money est perdue pour aller dans la mise
    public void Bet(int betValue)
    {
        Model.Money -= betValue;
        Model.PlayerBet += betValue;
    }

    public void LoseRound()
    {
        ResetBet();
    }

    public void WinRound()
    {
        Pay(Model.PlayerBet * 2);
        ResetBet();
    }

    public void DrawRound()
    {
        Pay(Model.PlayerBet);
        ResetBet();
    }

    void Player_Update(PlayerEventArgs e)
    {
        // Màj des labels
        if (this.textPseudo.text != e.Model.Pseudo) this.textPseudo.text = e.Model.Pseudo;
        if (this.textScore.text != e.Model.Score.ToString()) this.textScore.text = e.Model.Score.ToString();

        // Màj des mises
        this.MoneyComponent.GetComponent<Money>().textGold.text = e.Model.Money.ToString();
        this.MoneyComponent.GetComponent<Money>().textBet.text = e.Model.PlayerBet.ToString();

        if (Model.HasPlayed)
        {
            NotifyHasPlayed();
        }
        else
        {
            if (!GameProperties.Dealing) UpdatePlayerAreaColor(Color.white);
        }
    }

    void NotifyHasPlayed()
    {
        UpdatePlayerAreaColor(Color.green);
    }

    public void UpdatePlayerAreaColor(Color color)
    {
        if (!this.PlayerArea.GetComponent<SpriteRenderer>())
        {
            this.PlayerArea.GetComponent<Image>().color = color;
        }
        else
        {
            this.PlayerArea.GetComponent<SpriteRenderer>().color = color;
        }
    }

    void UpdatePlayerData()
    {
        this.textPseudo.text = Model.Pseudo;
        this.textScore.text = Model.Score.ToString();
        this.MoneyComponent.GetComponent<Money>().textGold.text = Model.Money.ToString();
        this.MoneyComponent.GetComponent<Money>().textBet.text = Model.PlayerBet.ToString();

        // _model.PlayerUpdate += Player_Update;
    }

    // Se clean et se remet à zéro
    public void PrepareNewRound()
    {
        this.Model.Score = 0;
        this.Hand.Reset();
        this.Model.isBurst = false;
        Destroy(Hand.InstantiatedPrefab);
    }

}
