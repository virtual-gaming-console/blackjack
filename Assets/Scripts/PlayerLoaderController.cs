﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLoaderController : MonoBehaviour
{
    public GameObject Panel;
    public Text TextPseudo;
    public RawImage Avatar;
    public Image Loader;
    public RectTransform _mainIcon;
    public float _timeStep;
    public float _oneStepAngle;
    float _startTime;
    public bool loading = true;


    // Start is called before the first frame update
    void Start()
    {
        _startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (loading && Time.time - _startTime >= _timeStep)
        {
            Vector3 iconAngle = _mainIcon.localEulerAngles;
            iconAngle.z += _oneStepAngle;

            _mainIcon.localEulerAngles = iconAngle;

            _startTime = Time.time;
        }
    }

    public void ControllerConnected()
    {
        loading = false;
        Loader.transform.localPosition = new Vector3(0, -210, 0);
        Loader.transform.rotation = new Quaternion(0, 0, 0, 0);
        Loader.GetComponent<Image>().color = Color.green;
    }
}
