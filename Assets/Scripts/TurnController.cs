﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * S'occupe de gérer un compte à rebours entre les tours.
 * Cette classe est principalement contrôlée depuis GameController
*/
public class TurnController : MonoBehaviour
{
    public int timeLeft;
    public Text countDownText;
    public GameController gameController;

    // On met à jour en permanence l'affichage
    void Update()
    {
        countDownText.text = timeLeft.ToString();
    }

    // Démarre un nouveau compte à rebours de durée time secondes
    public void StartTimer(int time)
    {
        StopAllCoroutines(); // Interromps tous les timer en cours
        timeLeft = time;
        StartCoroutine(countDown());
    }

    // La fonction qui fait s'écouler le temps
    IEnumerator countDown()
    {
        while (true)
        {
            // On attend toutes les secondes jusqu'à zéro
            yield return new WaitForSeconds(1f);
            timeLeft--;
            if (timeLeft == 0)
            {
                // Une fois à zéro on sort du while
                break;
            }
        }
        // Désactivé momentanément
        // On notifie que le timer a expiré
        // gameController.EndTurn();
    }

    // Arrête le timer en cours et n'en redémarre pas
    public void StopTimer()
    {
        StopAllCoroutines();
        timeLeft = 0;
    }
}
