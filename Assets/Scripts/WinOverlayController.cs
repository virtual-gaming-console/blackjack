﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinOverlayController : MonoBehaviour
{
    public Text WinnerPseudo;
    public RawImage WinnerAvatar;
    public GameObject fireworks;
    public GameObject InstantiatedFireworks;
    public GameObject WinMessagePrefab;
    public GameObject InstantiatedPrefab;
    public Button restartBtn;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Canvas>().worldCamera = Camera.main;
        GetComponent<Canvas>().sortingLayerName = "FrontLayer";

        if (GameProperties.ActualPlayerId != null)
        {
            PlayerModel player = GameProperties.GetPlayerModelWithMemberID(GameProperties.ActualPlayerId);
            WinnerPseudo.text = player.Pseudo;
            StartCoroutine(GameProperties.SetAvatarFromUrl(WinnerAvatar, player));
        }
        else
        {
            WinnerPseudo.text = GameProperties.Winner.Pseudo;
            StartCoroutine(GameProperties.SetAvatarFromUrl(WinnerAvatar, GameProperties.Winner));
        }

        // fireworks.GetComponent<ParticleSystem>().Play();
        InstantiatedFireworks = Instantiate(fireworks);

        InstantiatedPrefab = Instantiate(WinMessagePrefab);
        InstantiatedPrefab.GetComponent<MeshRenderer>().sortingLayerName = "FrontLayer";
        restartBtn.onClick.AddListener(delegate { RestartGame(); });
    }

    public void RestartGame()
    {
        if (GameProperties.isController)
        {
            GameProperties.SendMessage<ControllerActionMessage>(new ControllerActionMessage("restart-game", GameProperties.ActualPlayerId, null));
        }
    }

    void OnDestroy()
    {
        Destroy(this.InstantiatedPrefab);
        Destroy(this.InstantiatedFireworks);
    }
}
