﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Json;
using System.Text;
using UnityEngine.UI;

using NativeWebSocket;
using System.Net.Sockets;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public Player Player;
    bool _betting;
    public bool Betting
    {
        get => _betting;
        set
        {
            _betting = value;
            GameBettingStatusChanged(new GameBettingStatusEventArgs(value));
        }
    }
    public GameObject BetPanelPrefab;
    public GameObject InstantiatedPrefab;
    public Button hitBtn;
    public Button standBtn;
    public Button readyBtn;
    public event GameBettingStatusEventHandler GameBettingStatusChanged;
    public Text TextReady;
    public RawImage Avatar;
    public GameObject WinOverlayPrefab;
    public GameObject DefeatOverlayPrefab;
    public GameObject InstantiatedOverlay;

    // Start is called before the first frame update
    void Start()
    {
        GameBettingStatusChanged += Game_BettingStatusChanged;
        GameProperties.TriggerControllerAction += Trigger_ControllerAction;
        hitBtn.onClick.AddListener(delegate { Hit(); });
        standBtn.onClick.AddListener(delegate { Stand(); });
        readyBtn.onClick.AddListener(delegate { ReadyToContinue(); });
        Player.Model = GameProperties.GetPlayerModelWithMemberID(GameProperties.ActualPlayerId);
        StartCoroutine(GameProperties.SetAvatarFromUrl(Avatar, Player.Model));

        // Envoies du message pour annoncer la bonne initialization du controller
        GameProperties.SendMessage<ControllerActionMessage>(new ControllerActionMessage("controller-initialized", this.Player.Model.LobbyMemberID, null));
    }

    // Bouton Hit du GUI player scene
    public void Hit()
    {
        // Envoyer requête hit
        GameProperties.SendMessage<ControllerActionMessage>(new ControllerActionMessage("hit", Player.Model.LobbyMemberID, null));
    }

    // Bouton Stand du GUI player scene
    public void Stand()
    {
        // Envoyer requête stand
        GameProperties.SendMessage<ControllerActionMessage>(new ControllerActionMessage("stand", Player.Model.LobbyMemberID, null));
    }

    // Bouton Play agin du GUI player scene
    public void ReadyToContinue()
    {
        GameProperties.SendMessage<ControllerActionMessage>(new ControllerActionMessage("ready-next-round", Player.Model.LobbyMemberID, null));
    }

    void SetGameButtonsInteractable(bool trueorfalse)
    {
        this.standBtn.interactable = trueorfalse;
        this.hitBtn.interactable = trueorfalse;
    }

    public void Game_BettingStatusChanged(GameBettingStatusEventArgs args)
    {
        if (Betting)
        {
            SetGameButtonsInteractable(false); // Blocage interaction des boutons

            // Phase de mise on affiche le bet panel
            if (InstantiatedPrefab == null)
            {
                InstantiatedPrefab = Instantiate(BetPanelPrefab);
                var test = InstantiatedPrefab.GetComponent<BetPanelController>();
                InstantiatedPrefab.GetComponent<BetPanelController>().Initialize(Player);
            }
            else
            {
                Debug.Log("Already instantiated");
            }
        }
        else
        {
            // Fin de la phase de mise on assigne la mise et on cache le bet panel
            Destroy(InstantiatedPrefab);
            SetGameButtonsInteractable(true); // Déblocage des boutons
        }
    }

    void Trigger_ControllerAction(GameActionArgs e)
    {
        WSMessage msg = e.WsMessage;

        if (msg.data.action == "card-added-to-hand")
        {
            this.Player.Push(int.Parse(msg.data.content));
        }

        if (msg.data.action == "has-played")
        {
            SetGameButtonsInteractable(false);
        }

        if (msg.data.action == "betting-on")
        {
            Destroy(this.InstantiatedOverlay); // Retire l'overlay
            this.Betting = true;
            this.readyBtn.interactable = false; // Désactive le bouton play again
            this.TextReady.text = "";
            this.Player.PrepareNewRound();
        }

        if (msg.data.action == "betting-off")
        {
            this.Betting = false;
        }

        if (msg.data.action == "end-round")
        {
            this.readyBtn.interactable = true;
        }

        if (msg.data.action == "update-money")
        {
            this.Player.Model.Money = int.Parse(msg.data.content);
        }

        if (msg.data.action == "update-player-bet")
        {
            this.Player.Model.PlayerBet = int.Parse(msg.data.content);
        }

        if (msg.data.action == "ready")
        {
            this.readyBtn.interactable = false;
            this.TextReady.text = "You are ready";
        }

        if (msg.data.action == "burst")
        {
            this.Player.Burst();
            SetGameButtonsInteractable(false);
        }

        if (msg.data.action == "player-out")
        {
            this.Player.Model.Out = true;
            if (!InstantiatedOverlay)
            {
                InstantiatedOverlay = Instantiate(DefeatOverlayPrefab);
            }
        }

        if (msg.data.action == "player-win")
        {
            if (!InstantiatedOverlay)
            {
                InstantiatedOverlay = Instantiate(WinOverlayPrefab);
            }
        }
    }
}