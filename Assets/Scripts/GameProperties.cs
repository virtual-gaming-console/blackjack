﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.IO;
using NativeWebSocket;
using System.Text;
using UnityEngine.Networking;

/*
 * Classe static toujours accessible qui s'occupe des joueurs à initialiser au début de la partie
 * Cette classe est mise à jour par la scène d'écran titre puis utilisée dans la scene principale
*/
public static class GameProperties
{
    static int round = 1;
    static List<PlayerModel> listOfPlayers = new List<PlayerModel>();
    static int currentTurn = 1;
    public static int startingMoney = 500;
    static bool Betting = true;
    public static bool Dealing = false;
    public static bool GameIsRunning = false;
    public static PlayerModel Winner;
    // public static Dictionary<string, PlayerModel> PlayersByID = new Dictionary<string, PlayerModel>();
    public static event GameActionHandler TriggerGameAction;
    public static event GameActionHandler TriggerControllerAction;
    public static WebSocket ws;
    public static string ActualPlayerId;
    public static bool isController;
    public static bool Soloplayer = false;

    public static void SendMessage<T>(T message)
    {
        string json = JsonUtility.ToJson(message);

        GameProperties.ws.Send(Encoding.UTF8.GetBytes(json));
    }

    public static List<PlayerModel> GetPlayers()
    {
        return listOfPlayers;
    }

    public static int getRound()
    {
        return round;
    }

    public static void increaseRound()
    {
        round++;
    }

    // Retire du jeu le joueur avec l'ID correspondant
    public static void PlayerOut(int playerID)
    {
        listOfPlayers.Remove(GetPlayerModelByID(playerID));
    }

    public static void InitiateListOfPlayers(List<PlayerModel> listPlayer)
    {
        listOfPlayers = listPlayer;
    }

    // Attribue les lobbyMemberID aux joueurs
    public static void LinkPlayersToUnityGameObject(List<string> listIds)
    {
        for (int i = 0; i < listIds.Count; i++)
        {
            listOfPlayers[i].LobbyMemberID = listIds[i];
        }
    }

    public static PlayerModel GetActivePlayer()
    {
        return listOfPlayers[currentTurn - 1]; // Current turn = player iterator +1
    }

    public static PlayerModel GetPlayerModelWithMemberID(string id)
    {
        foreach (PlayerModel player in listOfPlayers)
        {
            if (player.LobbyMemberID == id)
            {
                return player;
            }
        }
        return null; // Pas possible normalement
    }

    public static void PlayerHasBet(int playerID)
    {
        PlayerModel player = GetPlayerModelByID(playerID);

        PlayerHasPlayed(playerID, true);
        // On incrémente le tour
        currentTurn++;
    }

    public static PlayerModel GetPlayerModelByID(int id)
    {
        foreach (PlayerModel player in listOfPlayers)
        {
            if (player.PlayerID == id)
            {
                return player;
            }
        }

        // Aucun joueur ne correspond
        return null;
    }


    public static void PlayerHasPlayed(int playerID, bool trueOrfalse)
    {
        GetPlayerModelByID(playerID).HasPlayed = trueOrfalse;

        if (EveryoneHasPlayed()) // Quand tout le monde a joué
        {
            if (Betting) // Si on est en phase de mise
            {
                ResetHasPlayedStatus();
                Betting = false;
                foreach (PlayerModel playerModel in listOfPlayers)
                {
                    GameProperties.SendMessage<ScreenActionMessage>(new ScreenActionMessage("betting-off", playerModel.LobbyMemberID, null));
                }
                // On distribue les cartes
            }
            else
            {
                // Si on est pas ne phase de mise c'est au dealer de jouer
            }
        }
    }

    public static void setBetting(bool trueOrFalse)
    {
        Betting = trueOrFalse;
    }

    public static bool getBettingStatus()
    {
        return Betting;
    }

    // Renvoies TRUE si tous les joueurs ont le statut hasPlayed = true
    public static bool EveryoneHasPlayed()
    {
        foreach (PlayerModel playerModel in listOfPlayers)
        {
            if (!playerModel.HasPlayed) return false;
        }

        return true;
    }

    public static void ResetHasPlayedStatus()
    {
        foreach (PlayerModel playerModel in listOfPlayers)
        {
            playerModel.HasPlayed = false;
        }
    }

    static void ResetCurrentTurn()
    {
        currentTurn = 1;
    }

    static void ResetAllBets()
    {
        foreach (PlayerModel player in listOfPlayers)
        {
            player.ResetBet();
        }
    }

    // Elimine les joueurs avec zéro money
    static void EliminatePlayers()
    {
        // Initialise la liste des ID à retirer
        List<int> IDToRemove = new List<int>();

        // Enregistrer l'ID de chaque joueur à éliminer
        foreach (PlayerModel player in listOfPlayers)
        {
            if (player.Money == 0) IDToRemove.Add(player.PlayerID);
        }

        // Retirer les joueurs de la liste
        foreach (int id in IDToRemove)
        {
            PlayerOut(id);
        }
    }

    public static void SetupNextRound(List<Player> listPlayers)
    {
        ResetAllBets();

        for (int i = 0; i < listPlayers.Count; i++)
        {
            listOfPlayers[i].Money = listPlayers[i].Model.Money;
            listOfPlayers[i].Out = listPlayers[i].Model.Out;
        }

        // On ne retire les joueurs à zéro
        EliminatePlayers();

        ResetCurrentTurn(); // réinitialise le compteur de tour
        increaseRound();
        Betting = true;
    }

    public static void ResetPlayers()
    {
        foreach (PlayerModel player in listOfPlayers)
        {
            player.Score = 0;
            player.Money = GameProperties.startingMoney;
            player.HasPlayed = false;
            player.PlayerBet = 0;
            player.Out = false;
            player.Score = 0;
            player.isBurst = false;
        }
    }

    public static IEnumerator SetAvatarFromUrl(RawImage image, PlayerModel playerModel)
    {
        if (playerModel.AvatarTexture)
        {
            image.color = Color.white;
            image.texture = playerModel.AvatarTexture;
            yield return null;
        }
        else
        {
            UnityWebRequest request = UnityWebRequestTexture.GetTexture(playerModel.AvatarUrl);
            yield return request.SendWebRequest();
            if (request.isNetworkError || request.isHttpError)
                Debug.Log(request.error);
            else
            {
                image.color = Color.white;
                image.texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
                playerModel.AvatarTexture = ((DownloadHandlerTexture)request.downloadHandler).texture;
            }
        }
    }

    public static void Action(WSMessage msg)
    {
        if (msg.title == "controller-action")
        {
            TriggerGameAction(new GameActionArgs(msg)); // Trigger un event
        }
        if (msg.title == "screen-action")
        {
            TriggerControllerAction(new GameActionArgs(msg)); // Trigger un event
        }
    }

}
