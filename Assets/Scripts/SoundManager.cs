﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * S'occupe de centraliser tous les sons
 * Peut être appelé à la volée
*/
public class SoundManager : MonoBehaviour
{
    public static AudioClip dealCardSound;
    public static AudioClip flipCradSound;
    public static AudioClip shuffleSound1;
    public static AudioClip shuffleSound2;
    public static AudioClip shuffleSound3;
    public static AudioClip loseSound;
    public static AudioClip winSound;
    public static AudioClip nopeSound;
    public static AudioClip drawSound;
    public static AudioClip bustedSound;
    public static AudioClip increaseBet;
    public static AudioClip bet;
    public static AudioSource audioSource;

    // Start is called before the first frame update
    void Awake()
    {
        // Correspondance avec les ressources du projet
        dealCardSound = Resources.Load<AudioClip>("Deal_card");
        flipCradSound = Resources.Load<AudioClip>("Flip_card");
        shuffleSound1 = Resources.Load<AudioClip>("Shuffle1");
        shuffleSound2 = Resources.Load<AudioClip>("Shuffle2");
        shuffleSound3 = Resources.Load<AudioClip>("Shuffle3");
        loseSound = Resources.Load<AudioClip>("Lose");
        winSound = Resources.Load<AudioClip>("Win");
        nopeSound = Resources.Load<AudioClip>("Hum_nope");
        drawSound = Resources.Load<AudioClip>("Draw");
        bustedSound = Resources.Load<AudioClip>("Busted");
        increaseBet = Resources.Load<AudioClip>("Bet");
        bet = Resources.Load<AudioClip>("Coins_dropped");

        audioSource = GetComponent<AudioSource>();
    }

    // Joue le son correspondant
    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "deal":
                audioSource.PlayOneShot(dealCardSound);
                break;
            case "flip":
                audioSource.PlayOneShot(flipCradSound);
                break;
            case "shuffle1":
                audioSource.PlayOneShot(shuffleSound1);
                break;
            case "shuffle2":
                audioSource.PlayOneShot(shuffleSound2);
                break;
            case "shuffle3":
                audioSource.PlayOneShot(shuffleSound3);
                break;
            case "lose":
                audioSource.PlayOneShot(loseSound);
                break;
            case "win":
                audioSource.PlayOneShot(winSound);
                break;
            case "nope":
                audioSource.PlayOneShot(nopeSound);
                break;
            case "draw":
                audioSource.PlayOneShot(drawSound);
                break;
            case "busted":
                audioSource.PlayOneShot(bustedSound);
                break;
            case "increaseBet":
                audioSource.PlayOneShot(increaseBet);
                break;
            case "bet":
                audioSource.PlayOneShot(bet);
                break;
        }
    }
}
