﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Controleur pour le prefab du joueur suivant
public class NextPlayerDestroy : MonoBehaviour
{
    // Temps avant la destruction de l'objet
    public float DestroyTime = 3f;

    // Start is called before the first frame update
    void Start()
    {
        // Il s'affiche, joue son animation puis et détruit
        Destroy(gameObject, DestroyTime);
    }
}