﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardStack : MonoBehaviour
{
    List<int> cards;

    public bool isGameDeck;

    /********* PREFABS **********/
    public GameObject textBustedPrefab;
    /****************************/

    public GameObject InstantiatedPrefab;

    public bool HasCards
    {
        get { return cards != null && cards.Count > 0; }
    }

    public event CardEventHandler CardRemoved;
    public event CardEventHandler CardAdded;

    void OnMouseDown()
    {
        Debug.Log("test click");
    }

    public int CardCount
    {
        get
        {
            if (cards == null)
            {
                return 0;
            }
            else
            {
                return cards.Count;
            }
        }
    }

    public IEnumerable<int> GetCards()
    {
        if (cards == null)
        {
            CreateDeck();
        }

        foreach (int i in cards)
        {
            yield return i;
        }
    }

    public int Pop()
    {
        if (cards.Count >= 0)
        {
            int cardDrawed = cards[0];
            cards.RemoveAt(0);

            if (CardRemoved != null)
            {
                CardRemoved(this, new CardEventArgs(cardDrawed));
            }

            return cardDrawed;
        }
        else
        {
            //TODO card stack is empty
            return 0;
        }
    }

    public void Push(int card)
    {
        cards.Add(card);

        if (CardAdded != null)
        {
            CardAdded(this, new CardEventArgs(card));
        }
    }

    public int HandValue()
    {
        int total = 0;
        int aces = 0;

        foreach (int card in GetCards())
        {
            int cardRank = card % 13;
            int cardValue = 0;

            //Les 8 premières cartes sont des numéros sont égale à leur valeur
            if (cardRank <= 8)
            {
                cardValue = cardRank + 2;
            }
            else
            {

                //Les visages valent 10 (cartes 9 à 11)
                if (cardRank > 8 && cardRank < 12)
                {
                    cardValue = 10;
                }
                else //L'as vaut 11
                {
                    aces++;
                }
            }

            //Calcul à chaque carte qui s'ajoute
            total += cardValue;
        }

        for (int i = 0; i < aces; i++)
        {
            if (total + 11 <= 21)
            {
                total += 11;
            }
            else
            {
                total += 1;
            }
        }

        return total;
    }

    public void CreateDeck()
    {
        // cards = new List<int>();
        if (cards != null)
        {
            cards.Clear();
        }

        for (int i = 0; i <= 51; i++)
        {
            cards.Add(i);
        }

        int n = cards.Count;

        while (n > 1)
        {
            n--;
            int k = Random.Range(0, n + 1);
            int temp = cards[k];
            cards[k] = cards[n];
            cards[n] = temp;
        }
    }

    void Awake()
    {
        cards = new List<int>();
        if (isGameDeck)
        {
            CreateDeck();
        }
    }

    // Remet à zéro le deck
    public void Reset()
    {
        // On enlève toutes les cartes
        while (this.cards.Count > 0)
        {
            this.Pop();
        }

        // Si c'est le main deck alors on mélange
        if (isGameDeck)
        {
            CreateDeck();
        }
    }
}
