﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

/*
 * Controleur principal du jeu de black jack
 * S'occupe de la synchronisation de toutes les autres classes
*/
public class GameController : MonoBehaviour
{
    public DealerController dealer;
    public CardStack deck;
    public TurnController turnController;

    public List<Player> listPlayerReferences;

    /************* PREFABS ************/
    public GameObject nextPlayerPrefab;
    /**********************************/

    public Button hitButton;
    public Button StandButton;
    public Button playAgainButton;
    public Text displayText;
    public Text roundCounter;

    public List<Player> players;
    public int playersTurn = 1;

    bool GameIsOver { get; set; }
    public bool GameOccuped = false;

    /********** MODIFIER LE TEMPS DES TOURS *********/
    public int RegularTurnTimer = 30;
    public int BettingTurnTimer = 15;
    /************************************************/

    public event GameEventHandler GameStatusChanged;
    public GameObject WinOverlayPrefab;
    public GameObject InstantiatedOverlay;

    // Distribue une carte au joueur
    public void Hit()
    {
        // On récupère le joueur actif
        Player player = GetActivePlayer();

        SoundManager.PlaySound("deal");
        player.Push(deck.Pop());
        // UpdatePlayerScore();
        if (player.Model.Score > 21)
        {
            SoundManager.PlaySound("nope");
            player.Burst();
            Stand();
        }
    }

    public int Hit(string id)
    {
        // On récupère le joueur actif
        Player player = GetPlayerWithMemberID(id);

        SoundManager.PlaySound("deal");
        int cardDrawed = deck.Pop();
        GameProperties.SendMessage<ScreenActionMessage>(new ScreenActionMessage("card-added-to-hand", id, cardDrawed.ToString()));
        player.Push(cardDrawed);

        if (player.Model.Score > 21)
        {
            SoundManager.PlaySound("nope");
            player.Burst();
            GameProperties.SendMessage<ScreenActionMessage>(new ScreenActionMessage("burst", id, null));
            Stand(id);
        }

        return cardDrawed;
    }

    // Le joueur actif passe son tour
    public void Stand()
    {
        NextPlayer();
    }

    // Le joueur avec l'id memberlobby indiqué passe son tour
    public void Stand(string id)
    {
        Player player = GetPlayerWithMemberID(id);

        player.Model.HasPlayed = true;

        if (GameProperties.EveryoneHasPlayed())
        {
            StartCoroutine(DealersTurn());
        }
    }

    // On passe au joueur suivant
    void NextPlayer()
    {
        playersTurn++;

        // Pour le moment on utilise pas les timer
        // if (GameProperties.Betting)
        // {
        //     turnController.StartTimer(BettingTurnTimer);
        // }
        // else
        // {
        //     turnController.StartTimer(RegularTurnTimer);
        // }

        if (playersTurn >= players.Count)
        {
            if (!GameProperties.getBettingStatus())
            {
                hitButton.interactable = false;
                StandButton.interactable = false;
                StartCoroutine(DealersTurn());
            }
            else
            {
                GameProperties.setBetting(false);
                playersTurn = 0;
            }
        }
        else
        {
            if (GetActivePlayer().Model.Out && playersTurn <= players.Count)
            {
                NextPlayer();
            }
            else
            {
                DisplayNextPlayer();
            }
        }
    }

    void Start()
    {
        players = new List<Player>();

        UpdateRound();
        InitiatePlayers();
        ResetRound();
        GameStatusChanged += game_StatusChanged;
        GameProperties.TriggerGameAction += Trigger_GameAction;
    }

    void OnDestroy()
    {
        GameStatusChanged -= game_StatusChanged;
        GameProperties.TriggerGameAction -= Trigger_GameAction;
    }

    void ResetRound()
    {
        GameProperties.GameIsRunning = true;
        StartCoroutine(BettingTime());
    }

    void UpdateRound()
    {
        roundCounter.text = GameProperties.getRound().ToString();
    }

    Player GetActivePlayer()
    {
        return players[playersTurn];
    }

    Player GetPlayerWithMemberID(string id)
    {
        foreach (Player player in players)
        {
            if (player.Model.LobbyMemberID == id)
            {
                return player;
            }
        }
        return null; // Pas possible normalement
    }

    void InitiatePlayers()
    {
        List<PlayerModel> listOfPlayers = GameProperties.GetPlayers();

        for (int i = 0; i < listOfPlayers.Count; i++)
        {
            Player newPlayer = listPlayerReferences[0]; // Je prends le premier de la liste
            listPlayerReferences.RemoveAt(0); // Puis je le retire pour garder dans la liste les gameobject disponibles

            newPlayer.Model = listOfPlayers[i];
            StartCoroutine(GameProperties.SetAvatarFromUrl(newPlayer.Avatar, newPlayer.Model));

            players.Add(newPlayer);
        }
        // Remet toutes les propriétés des joueurs par défaut en début de partie
        GameProperties.ResetPlayers();

        // Joueur solo
        if (players.Count == 1)
        {
            GameProperties.Soloplayer = true;
        }

        // Supprimer les gameobject des joueurs non attribués
        for (int i = (5 - listPlayerReferences.Count) + 1; i <= 5; i++)
        {
            Destroy(GameObject.Find("Player" + i));
            Destroy(GameObject.Find("MoneyP" + i));
            Destroy(GameObject.Find("Player" + i + "_area"));
            Destroy(GameObject.Find("Score p" + i));
            Destroy(GameObject.Find("Label p" + i));
            Destroy(GameObject.Find("Mask p" + i));
            Destroy(GameObject.Find("Avatar p" + i));
        }
    }

    void StartGame()
    {
        StartCoroutine(DispenseCards());
        GameProperties.Dealing = true; // Le deal commence
    }

    void EnableButtons()
    {
        hitButton.interactable = true;
        StandButton.interactable = true;
    }

    IEnumerator BettingTime()
    {
        GameProperties.setBetting(true);
        foreach (Player player in players)
        {
            GameProperties.SendMessage<ScreenActionMessage>(new ScreenActionMessage("betting-on", player.Model.LobbyMemberID, null));
        }
        PlayersNeedToPlay();

        // turnController.StartTimer(BettingTurnTimer);

        // Tant qu'on est en phase de mise, on attend
        while (GameProperties.getBettingStatus())
        {
            yield return new WaitForSeconds(1);
        }
        StartGame();
    }

    IEnumerator DispenseCards()
    {
        GameOccuped = true;
        // turnController.StopTimer();
        GameIsOver = false;
        yield return new WaitForSeconds(PlayShuffleSound());

        for (int i = 0; i < 2; i++)
        {
            foreach (Player player in players)
            {
                Hit(player.Model.LobbyMemberID);
                playersTurn++;
                yield return new WaitForSeconds(.3f);
            }

            // Old vcode version
            // for (int y = 0; y < players.Count; y++)
            // {
            //     Hit();
            //     playersTurn++;
            //     yield return new WaitForSeconds(.3f);
            // }
            playersTurn = 0;
            dealer.Hit(deck.Pop());
            yield return new WaitForSeconds(0.3f);
        }
        EnableButtons();
        // turnController.StartTimer(RegularTurnTimer);
        DisplayNextPlayer();
        GameOccuped = false;
        PlayersNeedToPlay();
    }

    // Joue un son shuffle au hasard parmi 3 et renvoie le temps d'attente correspondant
    float PlayShuffleSound()
    {
        int random = Random.Range(0, 3);
        float waiting = 0;
        if (random == 0)
        {
            SoundManager.PlaySound("shuffle1");
            waiting = 5.9f;
        }
        else if (random == 0)
        {
            SoundManager.PlaySound("shuffle2");
            waiting = 1.8f;
        }
        else
        {
            SoundManager.PlaySound("shuffle3");
            waiting = 2.8f;
        }
        return waiting;
    }

    public void PlayAgain()
    {
        // Avant de reload la scene il faudrait être sûr d'avoir enregistré correctement le statut des joueurs
        GameProperties.SetupNextRound(players);

        // Reset les joueurs
        players.ForEach(player => player.PrepareNewRound());

        // Reset le dealer
        dealer.PrepareNewRound();
        // Re mélanger le deck
        deck.Reset();
        // Reset l'état du jeu
        GameProperties.ResetHasPlayedStatus();
        SetGameIsOver(false);
        ResetRound();
    }

    void game_StatusChanged(object sender, GameEventArgs e)
    {
        if (e.GameIsOver)
        {
            //Partie terminée
            playAgainButton.interactable = true;

            foreach (Player player in players)
            {
                GameProperties.SendMessage<ScreenActionMessage>(new ScreenActionMessage("end-round", player.Model.LobbyMemberID, null));
            }
        }
        else
        {
            //On recommence
            playAgainButton.interactable = false;
        }
    }

    IEnumerator DealersTurn()
    {
        yield return new WaitForSeconds(1f);
        GameProperties.Dealing = false;
        GameProperties.ResetHasPlayedStatus();
        nextPlayerPrefab.GetComponent<TextMesh>().text = "Dealer's turn ";
        Instantiate(nextPlayerPrefab).GetComponent<MeshRenderer>().sortingOrder = 99;
        // turnController.StopTimer();
        yield return new WaitForSeconds(2f);
        StartCoroutine(dealer.FlipFirstCard());
        yield return new WaitForSeconds(2f);

        // Behavious against soloplayer
        if (GameProperties.Soloplayer)
        {
            bool dealerturn = true;
            int dealerScore = dealer.dealerHand.HandValue();
            while (dealerturn)
            {
                if (dealerScore >= players[0].Model.Score || dealerScore > 21)
                {
                    dealerturn = false;
                }

                if (dealerScore < players[0].Model.Score)
                {
                    dealer.Hit(deck.Pop());
                    yield return new WaitForSeconds(1f);
                }
            }
        }
        else
        {
            while (dealer.dealerHand.HandValue() < 17)
            {
                dealer.Hit(deck.Pop());
                yield return new WaitForSeconds(1f);
            }
        }

        if (dealer.dealerHand.HandValue() > 21)
        {
            dealer.Burst();
            yield return new WaitForSeconds(1f);
        }
        StartCoroutine(CheckScore());
    }

    IEnumerator CheckScore()
    {
        foreach (Player player in players)
        {
            if (!player.Model.isBurst)
            {
                int playerScore = player.Model.Score;
                int dealerScore = dealer.Score;

                if ((playerScore > 21 && dealerScore > 21) || playerScore == dealerScore)
                {
                    Draw(player);
                }
                else if (playerScore > 21 || (dealerScore >= playerScore && dealerScore <= 21))
                {
                    DealerWin(player);
                }
                else
                {
                    PlayerWin(player);
                }

                GameProperties.SendMessage<ScreenActionMessage>(new ScreenActionMessage("update-player-bet", player.Model.LobbyMemberID, "0"));
                GameProperties.SendMessage<ScreenActionMessage>(new ScreenActionMessage("update-money", player.Model.LobbyMemberID, player.Model.Money.ToString()));

                yield return new WaitForSeconds(2f);
            }
        }

        if (SomeoneWinTheGame())
        {
            StartCoroutine(EndTheGame());
        }
        else
        {
            SetGameIsOver(true);
        }
    }

    void DealerWin(Player player)
    {
        Log(player.Model.Pseudo + " has lost the round.");
        SoundManager.PlaySound("lose");
        player.LoseRound();
    }

    void PlayerWin(Player player)
    {
        Log(player.Model.Pseudo + " win the round!");
        SoundManager.PlaySound("win");
        player.WinRound();
    }

    void Draw(Player player)
    {
        Log("It's a draw for " + player.Model.Pseudo);
        SoundManager.PlaySound("draw");
        player.DrawRound();
    }

    public void SetGameIsOver(bool yesorno)
    {
        GameStatusChanged(this, new GameEventArgs(yesorno));
    }

    public void Log(string text)
    {
        displayText.text = text;
    }

    public void DisplayNextPlayer()
    {
        nextPlayerPrefab.GetComponent<TextMesh>().text = "It's your turn " + GetActivePlayer().Model.Pseudo;
        Instantiate(nextPlayerPrefab).GetComponent<MeshRenderer>().sortingOrder = 99;
    }

    public bool SomeoneWinTheGame()
    {
        if (GameProperties.Soloplayer)
        {
            PlayerModel soloPlayerModel = players[0].Model;
            if (soloPlayerModel.Money == 0)
            {
                GameProperties.Winner = new PlayerModel(0, "The dealer");
                GameProperties.Winner.AvatarUrl = "https://api.playdeviant.com/assets/images/avatars/x150.png";
                return true;
            }
            if (soloPlayerModel.Money >= 2000)
            {
                GameProperties.Winner = soloPlayerModel;
                return true;
            }
            return false;
        }

        List<Player> enLice = new List<Player>();

        foreach (Player player in players)
        {
            if (!player.Model.Out)
            {
                enLice.Add(player);
            }
        }

        if (enLice.Count == 1) // Il ne reste plus qu'un seul joueur
        {
            GameProperties.Winner = enLice[0].Model;
            return true;
        }
        else
        {
            return false;
        }
    }

    IEnumerator EndTheGame()
    {
        PlayerModel winner = GameProperties.Winner;

        nextPlayerPrefab.GetComponent<TextMesh>().text = winner.Pseudo + " WIN THE GAME !";
        Instantiate(nextPlayerPrefab).GetComponent<MeshRenderer>().sortingOrder = 99;

        yield return new WaitForSeconds(5f);
        // Envoie une requête pour swap la scene au controller qui a win
        GameProperties.SendMessage<ScreenActionMessage>(new ScreenActionMessage("player-win", GameProperties.Winner.LobbyMemberID, null));

        // SceneManager.LoadScene("VictoryScreen"); // Does not exist anymore
        InstantiatedOverlay = Instantiate(WinOverlayPrefab);
    }

    // Change la couleur des joueurs pour indiquer qu'ils n'ont pas joué
    public void PlayersNeedToPlay()
    {
        foreach (Player player in players)
        {
            player.UpdatePlayerAreaColor(Color.red);
        }
    }

    void CheckIfPlayersAreReadyToPlayAgain()
    {
        if (GameProperties.EveryoneHasPlayed())
        {
            PlayAgain();
        }
    }

    public void RestartBlackJack()
    {
        foreach (Player player in players)
        {
            GameProperties.SendMessage<ScreenActionMessage>(new ScreenActionMessage("update-money", player.Model.LobbyMemberID, "500"));
            player.Model = null;
        }
        GameProperties.Winner = null;
        SceneManager.LoadScene("BlackJackGame");
    }

    void Trigger_GameAction(GameActionArgs e)
    {
        // Actions permisent si le jeu n'est pas occupé
        if (!GameOccuped)
        {
            WSMessage msg = e.WsMessage;
            string LobbyMemberID = msg.data.member.id;

            // On ne traite rien si le jeu n'a pas commencé
            if (!GameProperties.GameIsRunning)
            {
                return;
            }
            // On ne traite rien si le joueur a déjà joué on termine la fonction
            if (GameProperties.GetPlayerModelWithMemberID(LobbyMemberID).HasPlayed)
            {
                return;
            }

            if (msg.data.action == "ready-next-round")
            {
                // Debug.Log("ready-next-round");
                GameProperties.GetPlayerModelWithMemberID(LobbyMemberID).HasPlayed = true;
                GameProperties.SendMessage<ScreenActionMessage>(new ScreenActionMessage("ready", LobbyMemberID, null));
                CheckIfPlayersAreReadyToPlayAgain();
            }

            if (msg.data.action == "restart-game")
            {
                RestartBlackJack();
            }

            /*** TEST OLD VERSION CONTROLLER ***/
            // if (msg.data.action == "play-again")
            // {
            //     GameProperties.GetPlayerModelWithMemberID(LobbyMemberID).HasPlayed = true;
            //     GameProperties.SendMessage<ScreenActionMessage>(new ScreenActionMessage("ready", LobbyMemberID, null));
            //     CheckIfPlayersAreReadyToPlayAgain();
            // }
            /***********************************/

            if (GameProperties.getBettingStatus())
            {
                int playerID = GameProperties.GetPlayerModelWithMemberID(LobbyMemberID).PlayerID;

                if (msg.data.action == "bet")
                {
                    GameProperties.GetPlayerModelByID(playerID).PlayerBet += int.Parse(msg.data.content);
                    GameProperties.SendMessage<ScreenActionMessage>(new ScreenActionMessage("update-player-bet", LobbyMemberID, GameProperties.GetPlayerModelByID(playerID).PlayerBet.ToString()));
                }

                if (msg.data.action == "bet-ok")
                {
                    GameProperties.PlayerHasBet(playerID);
                }

                if (msg.data.action == "ok")
                {
                    GameProperties.PlayerHasBet(playerID);
                }
            }
            else
            {
                if (msg.data.action == "hit")
                {
                    Hit(LobbyMemberID);
                }

                if (msg.data.action == "stand")
                {
                    Stand(LobbyMemberID);
                    GameProperties.SendMessage<ScreenActionMessage>(new ScreenActionMessage("has-played", LobbyMemberID, null));
                }
            }
        }
    }
}