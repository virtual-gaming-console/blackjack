﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * S'occupe de l'affichage des cardstack (toutes les mains)
 * 
*/
[RequireComponent(typeof(CardStack))]
public class CardStackView : MonoBehaviour
{
    CardStack deck;
    Dictionary<int, CardView> fetchedCards; // Chaque carte est associée à sa vue
    int lastCount;

    public Vector3 start; // Détermine l'emplacement de départ en jeu
    public float cardOffset; // Détermine l'espacement entre les carte dans un même cardstack
    public bool faceUp = false; // Détermine de quel côté tourner les cartes
    public bool reverseLayerOrder = false; // Affiche les cartes de droites à gauche ou l'inverse
    public GameObject cardPrefab;

    // Retourne la carte donnée en argument
    public void Toggle(int card, bool isFaceUp)
    {
        fetchedCards[card].IsFaceUp = isFaceUp;
    }

    void Awake()
    {
        fetchedCards = new Dictionary<int, CardView>();
        deck = GetComponent<CardStack>();
        ShowCards();
        lastCount = deck.CardCount;

        // Gestion des évènements
        deck.CardRemoved += deck_CardRemoved;
        deck.CardAdded += deck_CardAdded;
    }

    // Se trigger quand on ajoute une carte
    void deck_CardAdded(object sender, CardEventArgs e)
    {
        float co = cardOffset * deck.CardCount;
        Vector3 temp = start + new Vector3(co, 0f);
        AddCard(temp, e.CardIndex, deck.CardCount);
    }

    // Se trigger quand on retire une carte
    void deck_CardRemoved(object sender, CardEventArgs e)
    {
        Destroy(fetchedCards[e.CardIndex].Card);
        fetchedCards.Remove(e.CardIndex);
    }

    // Appelé à toutes les frames
    void Update()
    {
        if (lastCount != deck.CardCount) // Si il y a eu un changement
        {
            lastCount = deck.CardCount; // On met à jour l'index pour traquer le prochain changement
            ShowCards(); // On exécute la fonction d'affichage
        }
    }

    // Affiche les cartes en jeu à la bonne position
    public void ShowCards()
    {
        int cardCount = 0;

        if (deck.HasCards)
        {
            foreach (int i in deck.GetCards())
            {
                float co = cardOffset * cardCount;
                Vector3 temp = start + new Vector3(co, 0f);
                AddCard(temp, i, cardCount);
                cardCount++;
            }
        }
    }

    // Ajout d'une carte
    void AddCard(Vector3 position, int CardIndex, int positionIndex)
    {

        if (fetchedCards.ContainsKey(CardIndex))
        {
            if (!faceUp)
            { // Gère le côté à afficher
                CardModel model = fetchedCards[CardIndex].Card.GetComponent<CardModel>();
                model.ToggleFace(fetchedCards[CardIndex].IsFaceUp);
            }

            return;
        }

        GameObject cardCopy = (GameObject)Instantiate(cardPrefab);
        cardCopy.transform.position = position;

        CardModel cardModel = cardCopy.GetComponent<CardModel>();
        cardModel.CardIndex = CardIndex;
        cardModel.ToggleFace(faceUp);

        SpriteRenderer spriteRenderer = cardCopy.GetComponent<SpriteRenderer>();
        if (reverseLayerOrder)
        {
            spriteRenderer.sortingOrder = 51 - positionIndex;
        }
        else
        {
            spriteRenderer.sortingOrder = positionIndex;
        }
        // Notifie pour l'affichage
        fetchedCards.Add(CardIndex, new CardView(cardCopy));
    }
}
