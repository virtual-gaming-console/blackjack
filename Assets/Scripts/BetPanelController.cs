﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/*
 * S'occupe de gérer les mises des joueurs.
 * Cette classe est contrôlée par GameController.
*/
public class BetPanelController : MonoBehaviour
{
    public GameController gameController;
    public Text pseudoText;
    public Text totalBetValue;
    public Button SubmitBetBtn;
    public Button CancelBetBtn;
    public Button bet1;
    public Button bet10;
    public Button bet100;
    public int minBet;
    public int maxBet;
    private int _totalBet;
    public int Bet
    {
        get => _totalBet;
        set
        {
            if (value > maxBet)
            {
                _totalBet = maxBet;
            }
            else
            {
                _totalBet = value;
            }
        }
    }
    // Lobby member Id of the player
    public string playerRef;

    // Start is called before the first frame update
    void Awake()
    {
        // Disable(); // Désactive les boutons
        minBet = 50;
    }

    void Start()
    {
        Bet = 0;

        // Link les boutons aux fonctions correspondantes
        bet1.onClick.AddListener(delegate { Bet1(); });
        bet10.onClick.AddListener(delegate { Bet10(); });
        bet100.onClick.AddListener(delegate { Bet100(); });
        CancelBetBtn.onClick.AddListener(delegate { ResetBet(); });
        SubmitBetBtn.onClick.AddListener(delegate { SubmitBet(); });

        // Initialize(GameProperties.GetActivePlayer()); // Faut changer ça
    }

    // Update is called once per frame
    void Update()
    {
        totalBetValue.text = Bet.ToString();
    }

    public void Initialize(PlayerModel player)
    {
        // pseudoText.text = player.Pseudo; // Cette ligne génère une erreur
        maxBet = player.Money;
        playerRef = player.LobbyMemberID;
    }

    // Original behaviour
    public void Initialize(Player player)
    {
        if (pseudoText != null)
        {
            pseudoText.text = player.Model.Pseudo;
        }
        maxBet = player.Model.Money; // Empêche le joueur de miser plus que son total
        playerRef = player.Model.LobbyMemberID;
    }

    public void Bet1()
    {
        inscreaseBetSound();
        Bet += 1;
        UpdateBetOnScreen(1);
    }
    public void Bet10()
    {
        inscreaseBetSound();
        Bet += 10;
        UpdateBetOnScreen(10);
    }
    public void Bet100()
    {
        inscreaseBetSound();
        Bet += 100;
        UpdateBetOnScreen(100);
    }
    public void ResetBet()
    {
        Bet = 0;
        UpdateBetOnScreen(0);
    }

    void UpdateBetOnScreen(int bet)
    {
        GameProperties.SendMessage<ControllerActionMessage>(new ControllerActionMessage("bet", playerRef, bet.ToString()));
    }

    void inscreaseBetSound()
    {
        SoundManager.PlaySound("increaseBet");
    }

    // Transmet la mise du joueur au gameController
    public void SubmitBet()
    {
        // Si la mise du joueur est inférieur à la mise minimum
        if (Bet < minBet)
        {
            Bet = minBet; // La mise devient minimum
            GameProperties.SendMessage<ControllerActionMessage>(new ControllerActionMessage("bet", playerRef, Bet.ToString())); // màj côté screen
        }
        // Envoyer la requête pour confirmer la mise
        GameProperties.SendMessage<ControllerActionMessage>(new ControllerActionMessage("bet-ok", playerRef, null));
        SoundManager.PlaySound("bet");
    }

    // Désactive tous les boutons
    public void Disable()
    {
        bet1.interactable = false;
        bet10.interactable = false;
        bet100.interactable = false;
        CancelBetBtn.interactable = false;
        SubmitBetBtn.interactable = false;
    }

    //Active tous les boutons
    public void Enable()
    {
        bet1.interactable = true;
        bet10.interactable = true;
        bet100.interactable = true;
        CancelBetBtn.interactable = true;
        SubmitBetBtn.interactable = true;
    }
}
