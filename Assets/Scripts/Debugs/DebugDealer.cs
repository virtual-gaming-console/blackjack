﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Classe de test
public class DebugDealer : MonoBehaviour
{
    public CardStack dealer;
    public CardStack player;

    // int count = 0;
    // int[] cards = new int[] { 9, 12 };

    void OnGUI()
    {
        if (GUI.Button(new Rect(10, 10, 256, 28), "Hit me!"))
        {
            player.Push(dealer.Pop());
        }
    }

    public void Start()
    {
    }
}
