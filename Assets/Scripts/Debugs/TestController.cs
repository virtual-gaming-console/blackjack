﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestController : MonoBehaviour
{
    public bool EnableTest;
    public PlayerController controller;
    public int counter = 0;

    // Start is called before the first frame update
    void Start()
    {
        controller.Player.Model = new PlayerModel();
        controller.Player.Model.Pseudo = "Serge";
    }

    void OnGUI()
    {
        if (GUI.Button(new Rect(10, 150, 256, 28), "Test Win overlay"))
        {
            if (!controller.InstantiatedOverlay)
            {
                controller.InstantiatedOverlay = Instantiate(controller.WinOverlayPrefab);
            }
            else
            {
                Destroy(controller.InstantiatedOverlay);
            }
        }

        if (GUI.Button(new Rect(10, 175, 256, 28), "Defeat overlay"))
        {
            if (!controller.InstantiatedOverlay)
            {
                controller.DefeatOverlayPrefab.GetComponent<Canvas>().worldCamera = Camera.main;
                controller.InstantiatedOverlay = Instantiate(controller.DefeatOverlayPrefab);
            }
            else
            {
                Destroy(controller.InstantiatedOverlay);
            }
        }
    }

}
