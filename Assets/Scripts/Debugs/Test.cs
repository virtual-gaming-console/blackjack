﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

// Pour des test de dev
public class Test : MonoBehaviour
{
    public bool test5players; // Mettre oui pour générer 5 joueurs sans passer par l'écran titre
    public bool test1player; // Mettre oui pour générer 1 joueur sans passer par l'écran titre
    public bool enableSwitchScene;
    public Button switchSceneButton;

    // Start is called before the first frame update
    void Start()
    {
        if (!GameProperties.GameIsRunning)
        {
            if (test1player)
            {
                Test1player();
            }
            else
            {
                if (test5players)
                {
                    Test5players();
                }
            }

        }

        if (enableSwitchScene)
        {
            switchSceneButton.onClick.AddListener(delegate { SwitchScene(); });
        }
        else
        {
            Destroy(switchSceneButton);
        }
    }

    void Test1player()
    {
        List<PlayerModel> listOfModels = new List<PlayerModel>() { new PlayerModel(1, "player1") };
        GameProperties.InitiateListOfPlayers(listOfModels);
        // GameProperties.InitiateListOfPlayersWithPseudos(listOfPseudos);
    }

    void Test5players()
    {
        List<PlayerModel> listOfModels = new List<PlayerModel>() { new PlayerModel(1, "player1"), new PlayerModel(2, "player2"), new PlayerModel(3, "player3"), new PlayerModel(4, "player4"), new PlayerModel(5, "player5") };
        GameProperties.InitiateListOfPlayers(listOfModels);
        // GameProperties.InitiateListOfPlayersWithPseudos(listOfPseudos);
    }

    void SwitchScene()
    {
        SceneManager.LoadScene("Bet scene");
    }
}
