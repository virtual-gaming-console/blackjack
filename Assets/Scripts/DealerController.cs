﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * S'occupe du comportement du dealer
 * Cette classe est contrôlée par le gameController
*/
public class DealerController : MonoBehaviour
{

    public bool FirstCardFlipped { get; set; }
    public CardStack dealerHand;
    public CardStackView view;
    public int Score { get; set; }
    public int FirstCard { get; set; }
    public Text TextScore;
    public bool isBurst { get; set; }

    void Awake()
    {
        FirstCardFlipped = false; // Par défaut la première carte est face cachée
        dealerHand = GetComponent<CardStack>();
        view = dealerHand.GetComponent<CardStackView>();
    }

    // Retourne face visible la première acrte tirée par le dealer
    public IEnumerator FlipFirstCard()
    {
        if (!FirstCardFlipped)
        {
            yield return new WaitForSeconds(1f);
            SoundManager.PlaySound("flip");
            view.Toggle(FirstCard, true);
            view.ShowCards();
            UpdateScore(); // Rend visible le score
            FirstCardFlipped = true;
        }
    }

    // Fonction de tirage adapté au dealer
    public void Hit(int card)
    {
        SoundManager.PlaySound("deal");

        if (dealerHand.CardCount == 0) // Si le dealer n'a pas encore tiré de carte
        {
            FirstCard = card;
        }
        else if (dealerHand.CardCount == 1) // Si le dealer a déjà tiré une carte
        {
            if (card % 13 > 8) //c'est un visage ou un Dragon, on retourne la première carte
            {
                StartCoroutine(FlipFirstCard());
            }
        }

        dealerHand.Push(card);

        // à partir de la troisième carte, elle est posée face visible forcément
        if (dealerHand.CardCount >= 2)
        {
            view.Toggle(card, true);
        }

        if (dealerHand.CardCount > 2)
        {
            UpdateScore();
        }
    }

    // Affichage du préfab complètement décalé
    public void Burst()
    {
        GameObject prefabBurst = dealerHand.textBustedPrefab;
        this.isBurst = true;
        float ajustement = 0.8f;
        // Affichage et animation de défaite
        dealerHand.InstantiatedPrefab = (GameObject)Instantiate(prefabBurst, new Vector3(transform.position.x + ajustement, transform.position.y, ajustement), prefabBurst.transform.rotation);
        dealerHand.InstantiatedPrefab.GetComponent<MeshRenderer>().sortingOrder = 9;
        dealerHand.InstantiatedPrefab.GetComponent<MeshRenderer>().sortingLayerName = "MiddleFront";
        StartCoroutine(BurstSound());
    }

    // Joue le son correspondant
    IEnumerator BurstSound()
    {
        yield return new WaitForSeconds(0.33f);
        SoundManager.PlaySound("busted");
    }

    // Met à jour l'affichage à l'écran du score du dealer
    public void UpdateScore()
    {
        Score = dealerHand.HandValue();
        TextScore.text = Score.ToString();
    }

    // Se clean et se remet à zéro
    public void PrepareNewRound()
    {
        this.Score = 0;
        TextScore.text = Score.ToString();
        this.dealerHand.Reset();
        Destroy(dealerHand.InstantiatedPrefab);
        FirstCardFlipped = false;
        FirstCard = -1;
    }
}
