﻿using System;
using System.Runtime.Serialization;

[Serializable]
[DataContract]
public class ControllerAction
{
    [DataMember(Name = "action")]
    public string Action { get; set; }

    [DataMember(Name = "member")]
    public LobbyMember LobbyMember { get; set; }

    public override string ToString()
    {
        return $"[{Action}] by {LobbyMember.ToString()}";
    }
}