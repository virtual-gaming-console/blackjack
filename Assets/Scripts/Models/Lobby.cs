using System;
using System.Collections.Generic;

[Serializable]
public class Lobby
{
    public string id;

    public string code;

    public List<LobbyMember> members;

    public override string ToString()
    {
        string membersText = "";
        members.ForEach(member => membersText = $@"{membersText}
          - { member.ToString()}
        ");

        return $@"[{id}] {code}{membersText}";
    }
}