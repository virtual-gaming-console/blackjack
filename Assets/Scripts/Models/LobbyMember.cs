using System;

[Serializable]
public class LobbyMember
{
    public string id;

    public string nickname;

    public string picture;

    public bool isLobbyLeader;

    public override string ToString()
    {
        return $"[{id}] {nickname} as " + (isLobbyLeader ? "leader" : "member");
    }
}