using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

[Serializable]
[DataContract]
public class JoinRoomMessage : WSMessage
{
    public JoinRoomMessage(string lobbyCode, bool asController)
    {
        this.title = "join-room";
        this.data = new WSMessageData() { code = lobbyCode, isController = asController };
    }

    public JoinRoomMessage(string lobbyCode, bool asController, string memberId)
    {
        this.title = "join-room";
        this.data = new WSMessageData() { code = lobbyCode, isController = asController, memberId = memberId };
    }
}