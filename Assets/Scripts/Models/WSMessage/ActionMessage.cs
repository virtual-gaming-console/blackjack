using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

[Serializable]
[DataContract]
public class ActionMessage : WSMessage
{
    public ActionMessage(string action, string memberId, string content)
    {
        this.title = "action";
        this.data = new WSMessageData() { action = action, memberId = memberId, content = content };
    }
}