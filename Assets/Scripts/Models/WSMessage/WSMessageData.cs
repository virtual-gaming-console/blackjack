using System;
using System.Runtime.Serialization;

[Serializable]
[DataContract]
public class WSMessageData
{
    public string action;
    public LobbyMember member;

    public string memberId;

    public string code;

    public bool isController;

    public string content;

    public override string ToString()
    {
        string ret = $"-[IsController: {isController}]";
        if (action != null)
        {
            ret += $"-[Action: {action}]-";
        }
        if (member != null)
        {
            ret += $"-[Member: {member.ToString()}]-";
        }
        if (code != null)
        {
            ret += $"-[Code: {code}]-";
        }
        if (content != null)
        {
            ret += $"-[Content: {content}]-";
        }
        if (memberId != null)
        {
            ret += $"-[MemberId: {memberId}]-";
        }
        return ret;
    }
}