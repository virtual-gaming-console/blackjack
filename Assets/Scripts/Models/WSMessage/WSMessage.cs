using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

[Serializable]
public class WSMessage
{
    public string title;

    public WSMessageData data;

    public override string ToString()
    {
        return $"[{title}] {data.ToString()}";
    }
}