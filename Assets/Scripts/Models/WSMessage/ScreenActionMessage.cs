using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

[Serializable]
[DataContract]
public class ScreenActionMessage : ActionMessage
{
    public ScreenActionMessage(string action, string memberId, string content) : base(action, memberId, content)
    {
        this.title = "screen-action";
    }
}