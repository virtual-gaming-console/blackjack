using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

[Serializable]
[DataContract]
public class ControllerActionMessage : ActionMessage
{
    public ControllerActionMessage(string action, string memberId, string content) : base(action, memberId, content)
    {
        this.title = "controller-action";
    }
}