using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

[Serializable]
[DataContract]
public class TestMessage
{
    public string title;
    public string data;

    public override string ToString()
    {
        return $"[{title}] {data}";
    }
}