﻿using System.Collections.Generic;
using UnityEngine;

/*
 * Contient toutes les propriétés de l'objet joueur.
 * Va permettre d'enregistrer les datas des joueurs sans toucher les gameobject Unity.
*/
public class PlayerModel
{
    public int PlayerID { get; set; }

    public string AvatarUrl { get; set; }

    public Texture AvatarTexture { get; set; }
    string _pseudo;
    public string Pseudo
    {
        get => _pseudo;
        set
        {
            _pseudo = value;
            PlayerUpdate(new PlayerEventArgs(this));
        }
    }
    int _score;
    public int Score
    {
        get => _score;
        set
        {
            _score = value;
            PlayerUpdate(new PlayerEventArgs(this));
        }
    }

    int _money;
    public int Money
    {
        get => _money;
        set
        {
            _money = value;
            PlayerUpdate(new PlayerEventArgs(this));

        }
    }
    bool _hasPlayed;
    public bool HasPlayed
    {
        get => _hasPlayed;
        set
        {
            _hasPlayed = value;
            PlayerUpdate(new PlayerEventArgs(this));

        }
    }
    int _playerBet;
    public int PlayerBet
    {
        get => _playerBet;
        set
        {
            if (value <= 0)
            {
                _money += _playerBet;
                _playerBet = 0;
            }
            else
            {
                var diff = value - _playerBet;

                if (diff >= _money)
                {
                    diff = _money;
                    value = _playerBet + diff;
                }
                _playerBet = value;
                _money -= diff;
            }

            PlayerUpdate(new PlayerEventArgs(this));
        }
    }
    bool _out;
    public bool Out
    {
        get => _out;
        set
        {
            _out = value;
            // Envoie une requête pour faire switch d'écran le joueur
            if (_out && !GameProperties.isController)
            {
                GameProperties.SendMessage<ScreenActionMessage>(new ScreenActionMessage("player-out", this.LobbyMemberID, null));
            }
        }
    }
    public bool isBurst { get; set; }
    public string LobbyMemberID { get; set; }
    List<int> _hand;
    public bool IsConnected;

    public List<int> Hand
    {
        get => _hand;
        set
        {
            _hand = value;
            PlayerUpdate(new PlayerEventArgs(this));
        }
    }
    public event PlayerEventHandler PlayerUpdate;

    public PlayerModel(int id, string pseudo)
    {
        PlayerID = id;
        _pseudo = pseudo;
        _score = 0;
        _money = GameProperties.startingMoney;
        _hasPlayed = false;
        _playerBet = 0;
        Out = false;
        isBurst = false;
        LobbyMemberID = null;
        IsConnected = false;
    }

    public PlayerModel() : this(0, "no_name")
    {
    }

    public override string ToString()
    {
        return $"{Pseudo}: {Money}: {PlayerID}";
    }

    public void ResetBet()
    {
        _playerBet = 0;
        PlayerUpdate(new PlayerEventArgs(this));
    }

    // void UpdatePlayer()
    // {
    //     try
    //     {
    //         PlayerUpdate(new PlayerEventArgs(this));
    //     }
    //     catch (NullReferenceException ex)
    //     {
    //         string errMsg = ex.Message;
    //     }
    // }
}
